/**
 * Created by Louison Chevalier on 08/03/21.
 */

package com.flower.file.customview;

import java.util.List;
import com.flower.file.tflite.Classifier.Recognition;

public interface ResultsView {
  public void setResults(final List<Recognition> results);
}
